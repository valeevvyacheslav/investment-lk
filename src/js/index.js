import Popper from "popper.js";
import bootstrap from "bootstrap";
import "bootstrap-select";
import "jquery-circle-progress";
import "slick-carousel";

jQuery(function() {

    /* Sidebar */
    $('.overlay').on('click', function () {
        $('.sidebar').removeClass('active');
        $('.overlay').removeClass('active');
        $('#sidebarCollapse').toggleClass('open');
    });

    $('#sidebarCollapse').on('click', function () {
        $(this).toggleClass('open');
        $('.sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    /* Sidebar */

    /* Sticky nav */
    let navTop = $('.navbar-top-lp');

    if (navTop.length){
        let stickyNavTop = navTop.offset().top;

        let stickyNav = function(){

            let scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('.navbar-top-lp').addClass('sticky-top');
            } else {
                $('.navbar-top-lp').removeClass('sticky-top');
            }
        };

        stickyNav();
        $(window).scroll(function() {
            stickyNav();
        });
    }
    /* Sticky nav */

    /* Language select */
    let sel_lang =  $('.select-language');
    sel_lang.selectpicker({
        iconBase: 'icon'
    });
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        sel_lang.selectpicker('mobile');
    }
    /* Language select */

    /* Play Video */
    $('.play-video').on('click', function(e){
        e.preventDefault();
        let container = $(this).closest('.video__wrap');
        let video = container.find('iframe.video');

        container.addClass('active');
        video[0].src += "&autoplay=1";
    });
    /* Play Video */

    /* Popper */
    let reference = $('.faq__item__reference a');
    let popper = $('.faq__item__answer');
    if (popper.length){
        popper.hide();

        $(reference).on('click', function(e) {

            new Popper(
                $(this),
                $(this).closest('.faq__item').find('.faq__item__answer'),
                {
                    placement: 'bottom-start',
                    modifiers: {
                        offset: {
                            enabled: true,
                            offset: '0,20'
                        }
                    }
                }
            );
            $(this).closest('.faq__item').find('.faq__item__answer').toggle();
            e.preventDefault();
        });
    }
    /* Popper */

    /* Circle progress */
    $('.js-circle').circleProgress({
        'startAngle': Math.PI / 2,
        'emptyFill': '#cbcbce',
        'fill': '#fb6043'
    }).on('circle-animation-progress', function (event, progress, stepValue) {
        $(this).find('.value').text(Math.round(100 * stepValue) + '%');
    });
    /* Circle progress */

    /* Sliders and Carousel */
    let carouselPartners = $('#carousel-partners'),
        carouselRoadmap = $('#roadmap-carousel');
    if(carouselPartners.length){
        carouselPartners.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
            centerMode: true,
            variableWidth: true,
            prevArrow: '.partners__prev',
            nextArrow: '.partners__next',
        });
    }
    if(carouselRoadmap.length){
        carouselRoadmap.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '.roadmap__prev',
            nextArrow: '.roadmap__next',
            responsive: [
                {
                    breakpoint: 0,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 575.98,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 767.98,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                    }
                },
            ]
        });
    }
    /* Sliders and Carousel */
});
